var fs = require('fs');
var path = require('path');

var dir = process.argv[2] || './';
var extSearched = process.argv[3] || 'js';

fs.readdir(dir, function (err, files) {
    if (err) throw err;

    files.forEach(function (file) {
        var ext = path.extname(file);
        if (ext === '.'+extSearched) {
            console.log(file);
        }
    });
});