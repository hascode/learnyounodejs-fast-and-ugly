var http = require('http');

var url = process.argv[2] || 'http://www.hascode.com/';

http.get(url, function(res){
    res.setEncoding('UTF-8');
    var count = 0;

    var content = '';

    res.on('data', function(data){
        count += data.length;
        content += data;
    });

    res.on('end', function(){
        console.log(count);
        console.log(content);
    });
});