var http = require('http')
var map = require('through2-map')

var port = process.argv[2];

var server = http.createServer(function (req, res) {
    if (req.method !== 'POST') {
        res.end();
        return;
    }

    req.pipe(map(toUpper)).pipe(res);
})

function toUpper (chunk) {
    return chunk.toString().toUpperCase();
}

server.listen(port);