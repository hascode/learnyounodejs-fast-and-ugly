var mod = require('./l6_mod.js');

var dir = process.argv[2] || './';
var ext = process.argv[3] || 'js';

mod(dir, ext, function(err, data){
    if(err) throw err;

    data.forEach(function(file){
        console.log(file);
    });
});